﻿using Asynchronous_Weather_Data_Fetcher;
using Newtonsoft.Json;

//add key value
var apiKey = "f4de2e969d6a0a567f2ca31dab7ca0ad";
//create instance of WeatherData
var weatherData = new WeatherData { Location = "New York" };
await weatherData.FetchDataAsync(weatherData.Location, apiKey);

var jsonData = JsonConvert.SerializeObject(weatherData);
//create filename
var fileName = $"{weatherData.Location}.json";
//write all text
await File.WriteAllTextAsync(fileName, jsonData);