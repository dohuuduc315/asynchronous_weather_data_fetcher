﻿using Newtonsoft.Json;

namespace Asynchronous_Weather_Data_Fetcher
{
    public class WeatherData
    {
        public string Location { get; set; }
        public double Temperature { get; set; }
        public int Humidity { get; set; }
        public double WindSpeed { get; set; }
        public string Description { get; set; }

        public async Task FetchDataAsync(string city, string apiKey)
        {
            //create new HttpClient
            var client = new HttpClient();
            //make an asynchronous HTTP GET request to the API endpoint
            var response = await client.GetAsync($"https://api.openweathermap.org/data/2.5/weather?q={city}&appid={apiKey}&units=metric");
            //read the response content as the string
            var content = await response.Content.ReadAsStringAsync();
            //deserialize the JSON response
            dynamic? weatherData = JsonConvert.DeserializeObject(content);

            //assign value
            Location = weatherData.name;
            Temperature = weatherData.main.temp;
            Humidity = weatherData.main.humidity;
            WindSpeed = weatherData.wind.speed;
            Description = weatherData.weather[0].description;

            //print value
            Console.WriteLine($"Location: {Location}");
            Console.WriteLine($"Temperature: {Temperature}");
            Console.WriteLine($"Humidity: {Humidity}");
            Console.WriteLine($"WindSpeed: {WindSpeed}");
            Console.WriteLine($"Description: {Description}");
        }
    }
}
